
-- 模块功能：lcddemo
-- @module lcd
-- @author Dozingfiretruck
-- @release 2021.01.25

-- LuaTools需要PROJECT和VERSION这两个信息
PROJECT = "lcddemo"
VERSION = "1.0.0"

log.info("main", PROJECT, VERSION)

-- sys库是标配
_G.sys = require("sys")
spi_lcd = spi.deviceSetup(2, 7, 0, 0, 8, 40000000, spi.MSB, 1, 1)
log.info("lcd.init",
lcd.init("st7735s",{port = "device",pin_dc = 6, pin_rst = 10,direction = 2,w = 160,h = 80,xoffset = 0,yoffset = 24},spi_lcd))
lcd.invoff()

--添加硬狗防止程序卡死

if wdt then
    wdt.init(15000)--初始化watchdog设置为15s
    sys.timerLoopStart(wdt.feed, 10000)--10s喂一次狗
end



-- 初始化显示屏

--lcd.drawStr(50,35,"监听中",0X07FF)



--[[local bmp180 = require "bmp180"
i2cid = 0
i2c_speed = i2c.FAST
bmp180.init(i2cid)
sys.taskInit(function()
   i2c.setup(i2cid,i2c_speed)
   bmp180.init(i2cid)--初始化,传入i2c_id
   while 1 do
       
        local bmp180_data = bmp180.get_data()
        lcd.drawStr(50,10,"bmp180",0XE1FF)
        lcd.fill(0,22,160,80) 
        lcd.drawStr(0,30,"temp_温度:"..(bmp180_data.temp).."℃",0x0000)
        lcd.drawStr(0,50,"press_气压:"..(bmp180_data.press).."hPa",0x0000)
        lcd.drawStr(0,70,"high_高度:"..(bmp180_data.high).."m",0x0000)
        sys.wait(3000)
   end
end)]]
local bh1750 = require "bh1750"
local aht10 = require "aht10"
i2cid = 0
i2c_speed = i2c.FAST


sys.taskInit(function()
    local LEDA = gpio.setup(12, 0, gpio.PULLUP) -- PE07输出模式,内部上拉
    local LEDB = gpio.setup(13, 0, gpio.PULLUP) -- PE06输出模式,内部上拉
    --local LEDC = gpio.setup(pin.PD15, 0, gpio.PULLUP) -- PB10输出模式,内部上拉
    
        local count = 0
        while 1 do
            sys.wait(500)
            -- 一闪一闪亮晶晶
            LEDA(count % 3 == 0 and 1 or 0)
            LEDB(count % 3 == 1 and 1 or 0)
            
           -- log.info("gpio", "Go Go Go", count, rtos.bsp())
            count = count + 1
        end
    end)

    local num1, num2 = 0.1234, -1.5678

    rtc.set({year=2022,mon=5,day=25,hour=1,min=08,sec=43})
 

sys.taskInit(function()
    i2c.setup(i2cid,i2c_speed)
   --初始化,传入i2c_id
   -- local aht10_data = aht10.get_data()
   -- local count = 0
   aht10.init(i2cid)
  bh1750.init(i2cid)--初始化,传入i2c_id

  log.info("lvgl", lvgl.init()) --初始化 lvgl
  if lvgl.theme_set_act then
    -- 切换主题
    -- lvgl.theme_set_act("default")
    -- lvgl.theme_set_act("mono")
   --lvgl.theme_set_act("empty")
   --  lvgl.theme_set_act("material_light")
   --  lvgl.theme_set_act("material_dark")
    lvgl.theme_set_act("material_no_transition")
    -- lvgl.theme_set_act("material_no_focus")
   end


--lvgl.disp_set_bg_color(nil, 0x000000) --设置 lcd 背景色

 scr = lvgl.obj_create(nil, nil) --创建屏幕


 btn = lvgl.btn_create(scr) --创建按钮
lvgl.obj_set_size(btn,160,18)
lvgl.obj_align(btn, lvgl.scr_act(), lvgl.ALIGN_CENTER, 0,30)
local label = lvgl.label_create(btn) --创建按钮文字
--lvgl.label_set_text(label,"LuatOS!") --创建按钮文字，此处根据按键显示不同的模式


btn = lvgl.btn_create(scr)
lvgl.obj_set_size(btn,220,20)
lvgl.obj_align(btn, lvgl.scr_act(), lvgl.ALIGN_CENTER,0,-30)
local label_demo = lvgl.label_create(btn) --创建按钮文字
lvgl.label_set_recolor(label_demo,true)
lvgl.label_set_text(label_demo, "#00ff00 ath10+bh1750#") --创建按钮文字，此处根据按键显示不同的模式

local ATH10_x = -65
local ATH10_y = -10


local font = lvgl.font_get("opposans_m_12")



--[[local label_demo =lvgl.label_create(scr)
--lvgl.obj_set_size(label_time,120,25)
lvgl.obj_align(label_demo, lvgl.scr_act(), lvgl.ALIGN_CENTER, -130,ATH10_y+10)
--lvgl.label_set_text(label_time,"00:00:00")
lvgl.label_set_text(label_demo, "Air105-MP3/ATH10 demo")
]]

 label_hum =lvgl.label_create(scr)
--lvgl.obj_set_size(label_date,120,25)
lvgl.obj_align(label_hum, lvgl.scr_act(), lvgl.ALIGN_CENTER,ATH10_x+80 , ATH10_y)
--lvgl.label_set_text(label_hum,"0000-00-00") --创建按钮文字，此处用于显示日期

 label_temp =lvgl.label_create(scr)
--lvgl.obj_set_size(label_date,120,25)
lvgl.obj_align(label_temp, lvgl.scr_act(), lvgl.ALIGN_CENTER, ATH10_x, ATH10_y)
--lvgl.label_set_text(label_temp,"0000-00-00") --创建按钮文字，此处用于显示日期

label_light =lvgl.label_create(scr)
--lvgl.obj_set_size(label_date,120,25)
lvgl.obj_align(label_light, lvgl.scr_act(), lvgl.ALIGN_CENTER, ATH10_x, ATH10_y+20)

   --把传感器数据只保留小数点后两位并四舍五入
function keepDecimalTest(num, n)

    if type(num) ~= "number" then
        return num    
    end
    n = n or 2
    return string.format("%." .. n .. "f", num)
end
--lcd.setFont(lcd.font_opposansm12_chinese)


    while 1 do
        lvgl.label_set_text(label,os.date("%Y-%m-%d %H:%M:%S"))

         local aht10_data = aht10.get_data()
       num1=(aht10_data.RH*100)  --基于小米蓝牙温湿度计比对调的湿度原始数据+/-差异值或者直接修改传感器读取代码，确保串口和屏幕信息一致
       num2=(aht10_data.T)    --基于小米蓝牙温湿度计比对调的温度原始数据+/-差异值或者直接修改传感器读取代码，确保串口和屏幕信息一致
       humidity=keepDecimalTest(num1, 2)
       temp=keepDecimalTest(num2, 2)
  
    str_datetime = os.date()
    --print(str_datetime)

    local bh1750_data = bh1750.read_light()
    num3=(bh1750_data)
    light=keepDecimalTest(num3, 2)
    -- log.info("bh1750_read_light", bh1750_data)
     

    local str_hum ="湿度H:"..humidity.."%"
    local str_temp ="温度T:"..temp.." C"
    local str_light ="光强L:"..light.."nit"


  lvgl.label_set_text(label, os.date("%Y-%m-%d %H:%M:%S"))
    lvgl.label_set_text(label_hum, str_hum,font)
    lvgl.label_set_text(label_temp, str_temp,font)
    lvgl.label_set_text(label_light, str_light,font)
      
       
        
    lvgl.scr_load(scr)
    lvgl.obj_set_style_local_text_font(lvgl.scr_act(), lvgl.OBJ_PART_MAIN, lvgl.STATE_DEFAULT, font)
    sys.wait(100)
        
end


end)



-- 用户代码已结束---------------------------------------------
-- 结尾总是这一句
sys.run()
-- sys.run()之后后面不要加任何语句!!!!!
